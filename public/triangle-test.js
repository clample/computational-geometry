let points = [];
let canvas = new Canvas();

canvas.canvas.addEventListener("click", event => {
    if (points.length < 4) {
	let point = {x: event.offsetX, y: event.offsetY, z: 0};
	canvas.drawPoint(point);
	points[points.length] = point;
    } else {
	points = [];
	canvas.clearCanvas();
    }

    if (points.length === 3) {
	canvas.drawTriangle(points[0], points[1], points[2]);
    } else if (points.length == 4) {
	let isInside = inTriangle(points[0], points[1], points[2], points[3]);
	isInside ? canvas.drawText("Inside") : canvas.drawText("Outside");
    }
});
