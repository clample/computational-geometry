// points of polygon in counterclockwise order
let points = [];
let shouldClearCanvas = false;
let canvas = new Canvas();

canvas.canvas.addEventListener("click", event => {
    if (shouldClearCanvas) {
	canvas.clearCanvas();
	points = [];
	shouldClearCanvas = false;
	return;
    }
    let point = {x: event.offsetX, y: event.offsetY, z: 0};
    points.push(point);
    canvas.drawPoint(point);
});

let button = document.querySelector("button");
button.addEventListener("click", event => {
    shouldClearCanvas = true;
    let simplePolygon = new SimplePolygon(points);
    // TODO: For coloring the triangulation, we can use the dual graph?
    let triangulation = simplePolygon.triangulate();
    for (let triangle of triangulation) {
	canvas.drawPolygon(triangle);
    }
    canvas.drawPolygon(simplePolygon.vertices);
});
