let convexHull = new ConvexHull();
let canvas = new Canvas();

canvas.canvas.addEventListener("click", event => {
    let point = {x: event.offsetX, y: event.offsetY, z: 0};
    convexHull.incrementConvexHull(point);
    canvas.clearCanvas();
    for (let convexHullPoint of convexHull.convexHullPoints) {
	canvas.drawPoint(convexHullPoint);
    }
    for (let nonConvexHullPoint of convexHull.nonConvexHullPoints) {
	canvas.drawDisactivePoint(nonConvexHullPoint);
    }
    canvas.drawPolygon(convexHull.convexHullPoints);
});

let resetButton = document.getElementById("reset");
resetButton.addEventListener("click", event => {
    convexHull = new ConvexHull();
    canvas.clearCanvas();
});
