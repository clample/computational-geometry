class Canvas {

    constructor() {
	this._canvas = document.querySelector("canvas");
	this._context = this._canvas.getContext("2d");
    }

    get canvas() {
	return this._canvas;
    }

    get context() {
	return this._context;
    }

    drawPoint(point) {
	this.context.beginPath();
	this.context.arc(point.x, point.y, 5, 0, 2 * Math.PI);
	this.context.fill();
    }

    drawDisactivePoint(point) {
	this.context.beginPath();
	this.context.arc(point.x, point.y, 5, 0, 2 * Math.PI);
	this.context.stroke();
    }

    drawPolygon(points) {
	if (points.length < 2) {
            return;
	}

	this.context.beginPath();
	let startPoint = points[0]
	this.context.moveTo(startPoint.x, startPoint.y);
	for (let point of points.slice(1)) {
            this.context.lineTo(point.x, point.y);
	}
	this.context.lineTo(startPoint.x, startPoint.y);
	this.context.stroke();
    }

    fillPolygon(points) {
        if (points.length < 2) {
            return;
	}

	this.context.beginPath();
	let startPoint = points[0]
	this.context.moveTo(startPoint.x, startPoint.y);
	for (let point of points.slice(1)) {
            this.context.lineTo(point.x, point.y);
	}
	this.context.lineTo(startPoint.x, startPoint.y);
        this.context.save();
	this.context.fillStyle = 'rgba(255, 0, 0, 0.5)';
	this.context.fill();
	this.context.restore();

    }

    drawLine(point1, point2) {
        let start, end;
        if (point1.x - point2.x == 0) {
            start = {x: point1.x, y: 0};
            end = {x: point1.x, y: this.canvas.scrollHeight};
        } else if (point1.y - point2.y == 0) {
            start = {x: 0, y: point1.y};
            end = {x: this.canvas.scrollWidth, y: point1.y};
        } else {
            let slope = (point2.y - point1.y) / (point2.x - point1.x);
            let yIntercept = point2.y - slope * point2.x;
            if (yIntercept < 0) {
                start = {x: -yIntercept / slope, y: 0};
            } else if (0 <= yIntercept && yIntercept < this.canvas.scrollHeight) {
                start = {x: 0, y: yIntercept};
            } else {
                start = {
                    x: (this.canvas.scrollHeight - yIntercept) / slope,
                    y: this.canvas.scrollHeight
                };
            }

            let endYIntercept = slope * this.canvas.scrollWidth + yIntercept;
            if (endYIntercept < 0) {
                end = {x: -yIntercept / slope, y: 0};
            } else if (0 <= endYIntercept && endYIntercept < this.canvas.scrollHeight) {
                end = {
                    x: this.canvas.scrollWidth,
                    y: slope * this.canvas.scrollWidth + yIntercept
                };
            } else {
                end = {
                    x: (this.canvas.scrollHeight - yIntercept) / slope,
                    y: this.canvas.scrollHeight
                };
            }
            
        }

        
        this.context.beginPath();
        this.context.moveTo(start.x, start.y);
        this.context.lineTo(end.x, end.y);
        this.context.stroke();
    }

    drawTriangle(point1, point2, point3) {
	this.context.beginPath();
	this.context.moveTo(point1.x, point1.y);
	this.context.lineTo(point2.x, point2.y);
	this.context.lineTo(point3.x, point3.y);
	this.context.save();
	this.context.fillStyle = 'rgba(255, 0, 0, 0.5)';
	this.context.fill();
	this.context.restore();
    }

    clearCanvas() {
	let width = this.canvas.width;
	let height = this.canvas.height;
	this.context.clearRect(0, 0, width, height);
    }

    drawText(text) {
	this.context.font = '26px "Lucida Console"';
	this.context.fillText(text, 20, 30);
    }

}

function vector(point1, point2) {
    return {
        x: point2.x - point1.x,
        y: point2.y - point1.y,
        z: point2.z - point1.z,
    }
}
 
function crossProduct(vec1, vec2) {
    return {
        x: (vec1.y * vec2.z) - (vec1.z * vec2.y),
        y: (vec1.x * vec2.z) - (vec1.z * vec2.x),
        z: (vec1.x * vec2.y) - (vec1.y * vec2.x),
    }
}

function inTriangle(triangle1, triangle2, triangle3, point) {
    let orientedTriangle = orientClockwise(triangle1, triangle2, triangle3);
    return isRightTurn(orientedTriangle[0], orientedTriangle[1], point)
        && isRightTurn(orientedTriangle[1], orientedTriangle[2], point)
        && isRightTurn(orientedTriangle[2], orientedTriangle[0], point);
}

function orientClockwise(triangle1, triangle2, triangle3) {
    let isClockwise = isRightTurn(triangle1, triangle2, triangle3);
    if (isClockwise) {
        return [triangle1, triangle2, triangle3];
    } else {
        return [triangle1, triangle3, triangle2];
    }
}

function isRightTurn(point1, point2, point3) {
    return crossProduct(vector(point1, point2), vector(point2, point3)).z > 0;
}

function isLeftTurn(point1, point2, point3) {
    return crossProduct(vector(point1, point2), vector(point2, point3)).z < 0;
}

class SimplePolygon {

    // Take the vertices of the polygon in counterclockwise order
    // No check is done to ensure polygon is simple
    constructor(vertices) {
	if (vertices.length < 3) {
	    throw "Unable to create simple polygon with fewer than 3 vertices";
	}
        this._vertices = vertices;
    }

    get vertices() {
        return this._vertices;
    }

    triangulate() {
	if (this.vertices.length === 3) {
	    return [this.vertices];
	}
	let ear = this.findEar();
	let clonedVertices = this.vertices.slice();
	clonedVertices.splice(ear[1], 1);
	let remainingPolygon = new SimplePolygon(clonedVertices);
	let triangulation = remainingPolygon.triangulate();
	triangulation.push([
	    this.vertices[ear[0]],
	    this.vertices[ear[1]],
	    this.vertices[ear[2]]
	]);
	return triangulation;
    }

    findEar() {
	for (let i = 0; i < this.vertices.length; i++) {
	    if (this.isEar(i)) {
		return [mod(i-1, this.vertices.length), i, mod(i+1, this.vertices.length)];
	    }
	}
	throw "Unable to find ear for simple polygon: " + this.vertices;
    }

    isEar(index) {
        let vertex0 = this.vertices[mod(index - 1, this.vertices.length)]
        let vertex1 = this.vertices[index];
        let vertex2 = this.vertices[mod(index + 1, this.vertices.length)]

	// vertex1 is not convex and cannot be an ear
        if (!isLeftTurn(vertex0, vertex1, vertex2)) {
            return false;
        }

	for (let i = 0; i < this.vertices.length - 3; i++) {
	    let vertex = this.vertices[mod(index + 2 + i, this.vertices.length)];
	    if (inTriangle(vertex0, vertex1, vertex2, vertex)) {
		return false;
	    }
	}
	return true;
    }
        
}

class ConvexHull {
    
    constructor() {
        this._convexHullPoints = [];
        this._nonConvexHullPoints = [];
    }

    // Set of points of the convex hull (ordered counterclockwise)
    get convexHullPoints() {
        return this._convexHullPoints;
    }

    set convexHullPoints(val) {
        this._convexHullPoints = val;
    }

    // Set of points not in the convex hull (not ordered)
    get nonConvexHullPoints() {
        return this._nonConvexHullPoints;
    }

    isInConvexHull(point) {
        if (this.convexHullPoints.length < 3) {
            return false;
        }
        let centerPoint = this.interiorPoint();
        let i = this.findIndex(centerPoint, point);
        return isRightTurn(this.convexHullPoints[i], point, this.convexHullPoints[(i + 1) % this.convexHullPoints.length]);
    }

    // If the point is in the hull, insert it accordingly.
    // If not, add it to the list of nonConvexHullPoints.
    // This is performed in O(n), but could be sped up to O(log(n)) by using binary search.
    incrementConvexHull(point) {
        if (this.convexHullPoints.length <= 1) {
            this.convexHullPoints.push(point);
            return;
        }

        let centerPoint = this.interiorPoint();
        let i = this.findIndex(centerPoint, point);
        if (isRightTurn(this.convexHullPoints[i], point, this.convexHullPoints[(i + 1) % this.convexHullPoints.length])) {
            this.nonConvexHullPoints.push(point);
            return;
        }
    
        let {counterClockwiseTangent, clockwiseTangent} = this.findTangents(point, i);
        let newConvexHullPoints = [point];

        for (let j = 0; j < mod(counterClockwiseTangent - clockwiseTangent, this.convexHullPoints.length) - 1; j++) {
            this.nonConvexHullPoints.push(this.convexHullPoints[(j + clockwiseTangent + 1) % this.convexHullPoints.length]);
        }
    
        for (let j = 0; j <= mod(clockwiseTangent - counterClockwiseTangent, this.convexHullPoints.length); j++) {
            newConvexHullPoints.push(this.convexHullPoints[(j + counterClockwiseTangent) % this.convexHullPoints.length]);
        }
        this.convexHullPoints = newConvexHullPoints;
    }
    
    // Return some interior point of the convex hull (assumes a convex hull of at least two points)
    interiorPoint() {
        // given a convexHull of at least two points A, B the line AB is guaranteed to be contained in the convex hull
        // (by definition of convex hull)
        // The halfway point of this line is then an interior point of the convex hull.
        let a = this.convexHullPoints[0];
        let b = this.convexHullPoints[1];
        let vectorAB = vector(a, b);
        let pointAB = {
            x: a.x + (vectorAB.x * 0.5),
            y: a.y + (vectorAB.y * 0.5)
        }
        
        if (this.convexHullPoints.length == 2) {
            return pointAB;
        }

        // If the the line contains at least three points, we repeat the process again.
        // This ensures that the point is towards the center of the convex hull, and not on the edge.
        // This helps avoid edge conditions / issues with floating point precision
        let c = this.convexHullPoints[2];
        let vectorABC = vector(pointAB, c);
        return {
            x: pointAB.x + (vectorABC.x * 0.5),
            y: pointAB.y + (vectorABC.y * 0.5)
        }
    }

    // (assumes convex hull of at least two points)
    // Given a point inside the hull and a new point, we then determine which "slice" of the plane contains point
    // indicated by the index of the most clockwise point.
    // For visiualisation - imagine the convex hull as a pizza sliced around "centerPoint".
    findIndex(centerPoint, point) {
        for (let i = 0; i < this.convexHullPoints.length; i++) {
            if (isRightTurn(centerPoint, point, this.convexHullPoints[i]) && isLeftTurn(centerPoint, point, this.convexHullPoints[(i + 1) % this.convexHullPoints.length])) {
                return i;
            }
        }
    }

    // Given a point outside the convex hull and the index indicating the "slice" it's in,
    // return the two tangents from point to the convex hull
    // index gives the clockwise-most (lowest) point for the slice containing point.
    // convexHullPoints should be at least two points
    findTangents(point, index) {
        let counterClockwiseTangent = (index + 1) % this.convexHullPoints.length;
        while (isRightTurn(point, this.convexHullPoints[counterClockwiseTangent],
                           this.convexHullPoints[(counterClockwiseTangent + 1) % this.convexHullPoints.length])) {
            counterClockwiseTangent = (counterClockwiseTangent + 1) % this.convexHullPoints.length;
        }
        let clockwiseTangent = index;
        while (isLeftTurn(point, this.convexHullPoints[clockwiseTangent], this.convexHullPoints[mod(clockwiseTangent - 1, this.convexHullPoints.length)])) {
            clockwiseTangent = mod(clockwiseTangent - 1, this.convexHullPoints.length);
        }
        return {counterClockwiseTangent, clockwiseTangent};
    }
}

// Javascript will return negative values for "%". For example -1 % 2 = -1.
// For our purposes, we would like only positive values. For example -1 % 2 = 1.
function mod(n, m) {
  return ((n % m) + m) % m;
}

class Line {

    constructor(a, b) {
        this._a = a;
        this._b = b;
    }

    static fromPoints(p1, p2) {
    }
}
