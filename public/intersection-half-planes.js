let canvas = new Canvas();
let prevPoint = null;
let centerPoint = {x: canvas.canvas.scrollWidth / 2, y: canvas.canvas.scrollHeight / 2};
let convexHull = new ConvexHull();
let lines = [];

canvas.drawPoint(centerPoint);

let resetButton = document.getElementById("reset");
resetButton.addEventListener("click", event => {
    canvas.clearCanvas();
    prevPoint = null;
    convexHull = new ConvexHull();
    lines = [];
    canvas.drawPoint(centerPoint);
});

canvas.canvas.addEventListener("click", event => {
    let point = {x: event.offsetX, y: event.offsetY};
    if (prevPoint == null) {
        prevPoint = point;
        return;
    }

    // Get the line in transformed coordinates
    let line = getLine(transform(point), transform(prevPoint));
    if (line == null) {
        console.log("Unable to compute line for points.")
        prevPoint = null;
        return;
    }
    
    lines.push({start: prevPoint, end: point});
    prevPoint = null;
    convexHull.incrementConvexHull({x: line.a, y: line.b});

    canvas.clearCanvas();
    canvas.drawPoint(centerPoint);
    if (convexHull.isInConvexHull({x: 0, y: 0})) {
        canvas.fillPolygon(getBoundingPolygon());
    }
    for (let l of lines) {
        canvas.drawLine(l.start, l.end);
    }
});

// TODO: Be careful with -0
// transform to normal cartesian coordinates
function transform(point) {
    return {
        x: point.x - centerPoint.x,
        y: - (point.y - centerPoint.y)
    };
}

// transform to canvas coordinates
function untransform(point) {
    return {
        x: point.x + centerPoint.x,
        y: -point.y + centerPoint.y
    };
}

// return a line of the form ax+by=1
function getLine(point1, point2) {
    // Given point1 and point2, we have a system of two linear equations with variables a and b
    // We can solve for a and b with some linear algebra
    let det = point1.x * point2.y - point2.x * point1.y;
    if (det == 0) {
        return null;
    }
    return {
        a: (point2.y - point1.y) / det,
        b: (point1.x - point2. x) / det
    };
}

function getBoundingPolygon() {
    let boundingPolygon = [];
    for (let i=0; i < convexHull.convexHullPoints.length; i++) {
        let line = getLine(
            convexHull.convexHullPoints[i],
            convexHull.convexHullPoints[(i+1) % convexHull.convexHullPoints.length]
        );
        boundingPolygon.push(untransform({x: line.a, y: line.b}));
    }
    return boundingPolygon;
}
