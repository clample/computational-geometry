let points = [];
let canvas = new Canvas();
canvas.canvas.addEventListener("click", event => {
    if (points.length < 3) {
	let point = {x: event.offsetX, y: event.offsetY, z: 0};
	canvas.drawPoint(point);
	points[points.length] = point;
    } else {
	points = [];
	canvas.clearCanvas();
    }

    if (points.length === 3) {
	let result = crossProduct(vector(points[0], points[1]), vector(points[1], points[2]));
	if (result.z > 0) {
	    canvas.drawText("Right");
	} else if (result.z < 0) {
	    canvas.drawText("Left");
	} else {
	    canvas.drawText("Straight");
	}
    }
});
