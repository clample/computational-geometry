let points = [];
let shouldClearCanvas = false;
let canvas = new Canvas();

canvas.canvas.addEventListener("click", event => {
    if (shouldClearCanvas) {
	canvas.clearCanvas();
	points = [];
	shouldClearCanvas = false;
	return;
    }
    let point = {x: event.offsetX, y: event.offsetY, z: 0, isRemoved: false};
    canvas.drawPoint(point);
    points[points.length] = point;
});

let button = document.querySelector("button");
button.addEventListener("click", event => {
    shouldClearCanvas = true;
    for(let i = 0; i < points.length - 2; i++) {
	for (let j = 1; j < points.length - 1; j++) {
	    for (let k = 2; k < points.length; k++) {
		let pointI = points[i];
		let pointJ = points[j];
		let pointK = points[k];
		if (pointI.isRemoved || pointJ.isRemoved || pointK.isRemoved) {
		    continue;
		}
		for (let point of points) {
		    if (inTriangle(pointI, pointJ, pointK, point)) {
			point.isRemoved = true;
		    }
		}
	    }
	}
    }
    canvas.clearCanvas();
    for (let point of points) {
	if (!point.isRemoved) {
	    canvas.drawPoint(point);
	}
    }
});
